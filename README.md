## Views

There are 4 views in this application:

**Welcome**

 ![Alt text](img/welcome.png) 

**Honeycomb**

 ![Alt text](img/honeycomb.png) 

**Text editor**

 ![Alt text](img/editor.png) 

**Preferences**

 ![Alt text](img/preferences.png) 

---

## Shortcuts

Use the shortcuts to navigate between screens:

**Ctrl+O** - open text file

**Ctrl+S** - save currently opened text file

**Ctrl+P** - show preferences

**ESC** - exit from editor mode, preferences or iconify the app
