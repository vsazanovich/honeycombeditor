package com.github.geniot.honeycombeditor.actions;

import com.github.geniot.honeycombeditor.Constants;
import com.github.geniot.honeycombeditor.EditorFrame;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 08-08-2019
 */
public class LoadAction extends SaveAction implements ActionListener {

  public LoadAction(EditorFrame f) {
    super(f);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    //load
    JFileChooser fc;
    if (Constants.PROPS.containsKey(Constants.PROP_LAST_OPEN_DIR)) {
      try {
        fc = new JFileChooser(Constants.PROPS.getProperty(Constants.PROP_LAST_OPEN_DIR));
      } catch (Exception ex) {
        fc = new JFileChooser();
      }
    } else {
      fc = new JFileChooser();
    }

    fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    FileNameExtensionFilter filter = new FileNameExtensionFilter("Normal text file (*.txt)", "txt", "text");
    fc.setFileFilter(filter);

    int returnVal = fc.showOpenDialog(frame);
    Constants.PROPS.setProperty(Constants.PROP_LAST_OPEN_DIR, fc.getCurrentDirectory().getPath());
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      if (frame.openFileName != null) {
        saveText();
        frame.openFileName = null;
        frame.textEditor.textArea.setText("");
      }

      File selectedFile = fc.getSelectedFile();
      if (!selectedFile.exists()) {
        try {
          selectedFile.createNewFile();
        } catch (IOException ex) {
          ex.printStackTrace();
        }
      }
      load(selectedFile);

    }

  }

  public void load(File selectedFile) {
    frame.openFileName = selectedFile.getAbsolutePath();
    frame.shortFileName = selectedFile.getName();
    try {
      BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(selectedFile), "UTF-8"));
      try {
        StringBuilder sb = new StringBuilder();
        String line = br.readLine();

        while (line != null) {
          sb.append(line);
          line = br.readLine();
          if (line != null) {
            sb.append(System.lineSeparator());
          }
        }

        String text = sb.toString();
        frame.textEditor.hash = Constants.getHash(text);
        frame.textEditor.textArea.setText(text);
        frame.honeyCombPanel.updateButtonStates();
        frame.setTitle(frame.shortFileName);
        ((CardLayout) frame.cards.getLayout()).show(frame.cards, Constants.PROP_HONEYCOMB_COMPONENT);

      } finally {
        br.close();
      }
    } catch (Exception e1) {
      e1.printStackTrace();
    }
  }
}

