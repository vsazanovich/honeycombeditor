package com.github.geniot.honeycombeditor.actions;

import com.github.geniot.honeycombeditor.Constants;
import com.github.geniot.honeycombeditor.EditorFrame;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class UnloadAction extends SaveAction implements ActionListener {
  public UnloadAction(EditorFrame f) {
    super(f);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (frame.openFileName != null) {
      saveText();
      frame.textEditor.textArea.setText("");
      frame.textEditor.pager.setText("");
      frame.openFileName = null;
      frame.setTitle("Honeycomb");
      ((CardLayout) frame.cards.getLayout()).show(frame.cards, Constants.PROP_WELCOME_COMPONENT);
    }
  }
}
