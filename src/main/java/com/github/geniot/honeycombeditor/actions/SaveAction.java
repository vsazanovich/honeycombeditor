package com.github.geniot.honeycombeditor.actions;

import com.github.geniot.honeycombeditor.Constants;
import com.github.geniot.honeycombeditor.EditorFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.io.*;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Stream;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 08-08-2019
 */
public class SaveAction extends WindowAdapter implements ActionListener {
  protected EditorFrame frame;
  private ExecutorService executorService = Executors.newCachedThreadPool();
  SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

  public SaveAction(EditorFrame f) {
    this.frame = f;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    saveText();
  }

  public void saveText() {
    int newHash = Constants.getHash(frame.textEditor.textArea.getText());
    if (newHash != frame.textEditor.hash) {
      final long time = System.currentTimeMillis();
      try {
        Writer writer = new OutputStreamWriter(new FileOutputStream(frame.openFileName), "UTF-8");
        BufferedWriter fout = new BufferedWriter(writer);
        fout.write(frame.textEditor.textArea.getText());
        fout.close();
        frame.textEditor.hash = newHash;
      } catch (IOException ex) {
        ex.printStackTrace();
      }
      frame.setTitle(frame.shortFileName + " - " + format.format(new Date(time)));
      executorService.execute(new Runnable() {
        @Override
        public void run() {
          saveAsync(time);
        }
      });
    }
  }

  private void saveAsync(long time) {
    try {
      Path path = Paths.get(frame.openFileName + ".zip");
      Map<String, String> zipProps = new HashMap<>();
      zipProps.put("create", "true");
      URI uri = URI.create("jar:" + path.toUri());

      if (frame.zipFS == null || !frame.zipFS.isOpen()) {
        frame.zipFS = FileSystems.newFileSystem(uri, zipProps);
      }
      cleanup();
      backup(time);
      frame.zipFS.close();

      System.out.println("Backup took " + (System.currentTimeMillis() - time));

    } catch (Exception ex) {
      frame.setTitle(ex.getMessage());
      ex.printStackTrace();
    }
  }


  private void cleanup() {
    try {
      SortedSet<Path> names = new TreeSet<>();
      Stream<Path> stream = Files.list(frame.zipFS.getPath("/"));
      stream.forEach((path -> {
        names.add(path.getFileName());
      }));
      if (names.size() >= Constants.BACKUP_LIMIT) {
        while (names.size() >= Constants.BACKUP_LIMIT) {
          Path path2delete = names.first();
          Files.delete(path2delete);
          names.remove(path2delete);
          System.out.println("removed " + path2delete);
        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  private void backup(long timestamp) {
    try {
      Path nf = frame.zipFS.getPath(timestamp + ".txt");
      try (Writer writer = Files.newBufferedWriter(nf, StandardCharsets.UTF_8, StandardOpenOption.CREATE)) {
        writer.write(frame.textEditor.textArea.getText());
      }
      System.out.println("added " + nf);
//      fs.close();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }
}


