package com.github.geniot.honeycombeditor;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 08-08-2019
 */
public class Constants {
  public static final String DEFAULT_LAF = "com.jtattoo.plaf.noire.NoireLookAndFeel";

  public static Properties PROPS = new Properties();

  public static final String PROP_LAF = "PROP_LAF";

  public static final String PROP_WIDTH = "PROP_WIDTH";
  public static final String PROP_HEIGHT = "PROP_HEIGHT";
  public static final String PROP_POS_X = "PROP_POS_X";
  public static final String PROP_POS_Y = "PROP_POS_Y";
  public static final String PROP_DIVIDER_LOCATION = "PROP_DIVIDER_LOCATION";

  public static final String PROP_LAST_OPEN_FILE = "PROP_LAST_OPEN_FILE";
  public static final String PROP_LAST_OPEN_DIR = "PROP_LAST_OPEN_DIR";

  public static final String PROP_HONEYCOMB_COMPONENT = "PROP_HONEYCOMB_COMPONENT";
  public static final String PROP_TEXTEDITOR_COMPONENT = "PROP_TEXTEDITOR_COMPONENT";
  public static final String PROP_WELCOME_COMPONENT = "PROP_WELCOME_COMPONENT";
  public static final String PROP_PREFS_COMPONENT = "PROP_PREFS_COMPONENT";

  public static final String PROP_FONT_SIZE = "PROP_FONT_SIZE";

  public static final String PROP_ENABLED_COLOR = "PROP_ENABLED_COLOR";
  public static final String PROP_DISABLED_COLOR = "PROP_DISABLED_COLOR";

  public static final int PAGE_SIZE = 1000;
  public static final int BACKUP_LIMIT = 20;

  public static final String PROPS_FILE_NAME = "honeycombeditor.properties";


  public static int getHash(String str) {
    try {
      MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
      messageDigest.update(str.getBytes());
      String encryptedString = new String(messageDigest.digest());
      return encryptedString.hashCode();
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    return str.hashCode();
  }
}
