package com.github.geniot.honeycombeditor.panels;

import com.github.geniot.honeycombeditor.EditorFrame;

import javax.swing.*;
import java.awt.*;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 08-08-2019
 */
public class EditorButton extends JButton {
  private EditorFrame frame;

  public EditorButton(EditorFrame f){
    super("");
    this.frame = f;
    setFocusPainted(false);
    setMargin(new Insets(1, 1, 1, 1));
  }
}
