package com.github.geniot.honeycombeditor.panels;

import com.github.geniot.honeycombeditor.Constants;
import com.github.geniot.honeycombeditor.EditorFrame;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.SplitPaneUI;
import javax.swing.plaf.basic.BasicSplitPaneUI;
import java.awt.*;
import java.awt.event.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 08-08-2019
 */
public class TextEditor extends JPanel {
  public int hash;
  private EditorFrame editorFrame;
  public JTextArea textArea;
  public JProgressBar hundredProgressBar;
  public JLabel pager;
  public JPanel statusPanel;
  public JSplitPane splitPane;
  private ExecutorService executorService = Executors.newCachedThreadPool();

  public TextEditor(EditorFrame f) {
    this.editorFrame = f;
    this.setName(Constants.PROP_TEXTEDITOR_COMPONENT);
    this.setLayout(new BorderLayout());
    this.addComponentListener(new ComponentAdapter() {
      @Override
      public void componentShown(ComponentEvent e) {
        super.componentShown(e);
        updateStatus();
        textArea.requestFocus();
      }
    });

    textArea = new JTextArea();
    textArea.setWrapStyleWord(true);
    textArea.setLineWrap(true);
    textArea.setEditable(true);
    float size = Constants.PROPS.containsKey(Constants.PROP_FONT_SIZE) ? Float.parseFloat(Constants.PROPS.getProperty(Constants.PROP_FONT_SIZE)) : 20f;
    textArea.setFont(textArea.getFont().deriveFont(size)); // will only change size to 12pt
    textArea.setMargin(new Insets(10, 10, 50, 10)); // tt is JTextArea instance
    textArea.getCaret().setBlinkRate(0);
    textArea.setFocusable(true);
    textArea.addCaretListener(new CaretListener() {
      @Override
      public void caretUpdate(CaretEvent e) {
        if (getCurrentPage() != getTotalPages()) {
          splitPane.setDividerLocation(editorFrame.getHeight());
        } else {
          splitPane.setDividerLocation(getDividerLocation());
        }
        updateStatus();
      }
    });

    textArea.getDocument().addDocumentListener(new DocumentListener() {
      @Override
      public void insertUpdate(DocumentEvent e) {
        updateStatus();
      }

      @Override
      public void removeUpdate(DocumentEvent e) {
        updateStatus();
      }

      @Override
      public void changedUpdate(DocumentEvent e) {
        updateStatus();
      }
    });

    textArea.addMouseWheelListener(new MouseWheelListener() {
      @Override
      public void mouseWheelMoved(MouseWheelEvent e) {
        if (e.getModifiers() == InputEvent.CTRL_MASK) {
          float size = Constants.PROPS.containsKey(Constants.PROP_FONT_SIZE) ? Float.parseFloat(Constants.PROPS.getProperty(Constants.PROP_FONT_SIZE)) : 20f;
          size += ((float) e.getUnitsToScroll()) / 10;
          textArea.setFont(textArea.getFont().deriveFont(size));
          Constants.PROPS.setProperty(Constants.PROP_FONT_SIZE, String.valueOf(size));
        }
      }
    });

    JScrollPane scrollPane = new JScrollPane(textArea);
    scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
    scrollPane.setBorder(null);

    JTextArea dummy = new JTextArea();
    dummy.addFocusListener(new FocusAdapter() {
      @Override
      public void focusGained(FocusEvent e) {
        super.focusGained(e);
        if (splitPane.getDividerSize() == 0) {
          splitPane.setDividerSize(5);
        } else {
          splitPane.setDividerSize(0);
        }
        textArea.requestFocus();
      }
    });
    splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, scrollPane, dummy);
    splitPane.setDividerSize(0);
    splitPane.setDividerLocation(getDividerLocation());

    SplitPaneUI spui = splitPane.getUI();
    if (spui instanceof BasicSplitPaneUI) {
      ((BasicSplitPaneUI) spui).getDivider().addMouseListener(new MouseAdapter() {
        public void mouseReleased(MouseEvent e) {
          Constants.PROPS.setProperty(Constants.PROP_DIVIDER_LOCATION, String.valueOf(splitPane.getDividerLocation()));
        }
      });
    }

    add(splitPane, BorderLayout.CENTER);


    statusPanel = new JPanel();
    statusPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
    statusPanel.setPreferredSize(new Dimension(getWidth(), 30));
    statusPanel.setLayout(new BorderLayout());
//        statusLabel = new JLabel();

    hundredProgressBar = new JProgressBar(0, 100);
    hundredProgressBar.setStringPainted(true);

    pager = new JLabel("", JLabel.CENTER);
    Dimension dim = new Dimension(60, 10);
    pager.setMinimumSize(dim);
    pager.setMaximumSize(dim);
    pager.setPreferredSize(dim);

    statusPanel.add(hundredProgressBar, BorderLayout.CENTER);
    statusPanel.add(pager, BorderLayout.EAST);
    add(statusPanel, BorderLayout.SOUTH);
  }

  private int getDividerLocation() {
    int divLoc = editorFrame.getHeight() - 40;
    if (Constants.PROPS.containsKey(Constants.PROP_DIVIDER_LOCATION)) {
      divLoc = Integer.parseInt(Constants.PROPS.getProperty(Constants.PROP_DIVIDER_LOCATION));
    }
    return divLoc;
  }

  public int getCurrentPage() {
    return textArea.getCaretPosition() / Constants.PAGE_SIZE + 1;
  }

  public int getTotalPages() {
    return textArea.getText().length() / Constants.PAGE_SIZE + 1;
  }

  private void updateStatus() {
    executorService.execute(() -> updateStatusInternal());
  }

  protected void updateStatusInternal() {
    pager.setText(String.valueOf(getCurrentPage()) + '/' + getTotalPages());
    int percent = 100;
    if (getCurrentPage() == getTotalPages()) {
      percent = (textArea.getText().length() % Constants.PAGE_SIZE) * 100 / Constants.PAGE_SIZE;
    }
    hundredProgressBar.setValue(percent);
  }
}
