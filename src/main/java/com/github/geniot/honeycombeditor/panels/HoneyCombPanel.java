package com.github.geniot.honeycombeditor.panels;

import com.github.geniot.honeycombeditor.ColorUtils;
import com.github.geniot.honeycombeditor.Constants;
import com.github.geniot.honeycombeditor.EditorFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 08-08-2019
 */
public class HoneyCombPanel extends JPanel implements ActionListener {
    private EditorFrame frame;
    private List<EditorButton> buttons = new ArrayList<>();

    public HoneyCombPanel(EditorFrame f) {
        this.frame = f;
        this.setName(Constants.PROP_HONEYCOMB_COMPONENT);

        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                super.componentShown(e);
                updateButtonStates();
            }
        });

        for (int i = 1; i <= 1024; i++) {
            EditorButton eb = new EditorButton(frame);
            eb.setName(String.valueOf(i));
            add(eb);
            eb.addActionListener(this);
            buttons.add(eb);
        }
        setLayout(new GridLayout(32, 32));

    }

    public void updateButtonStates() {
        Color enabledColor = Constants.PROPS.containsKey(Constants.PROP_ENABLED_COLOR) ? ColorUtils.getColorByName(Constants.PROPS.getProperty(Constants.PROP_ENABLED_COLOR)) : Color.ORANGE;
        Color disabledColor = Constants.PROPS.containsKey(Constants.PROP_DISABLED_COLOR) ? ColorUtils.getColorByName(Constants.PROPS.getProperty(Constants.PROP_DISABLED_COLOR)) : Color.BLACK;
        for (int i = 0; i < buttons.size(); i++) {
            if (i < frame.textEditor.getTotalPages()) {
                buttons.get(i).setEnabled(true);
                buttons.get(i).setBackground(enabledColor);
            } else {
                buttons.get(i).setEnabled(false);
                buttons.get(i).setBackground(disabledColor);
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int pageNum = Integer.parseInt(((EditorButton) e.getSource()).getName());
        if (pageNum == frame.textEditor.getTotalPages()) {
            frame.textEditor.textArea.setCaretPosition(frame.textEditor.textArea.getText().length());
        } else {
            frame.textEditor.textArea.setCaretPosition((pageNum - 1) * Constants.PAGE_SIZE);
        }
        ((CardLayout) frame.cards.getLayout()).show(frame.cards, Constants.PROP_TEXTEDITOR_COMPONENT);
    }
}
