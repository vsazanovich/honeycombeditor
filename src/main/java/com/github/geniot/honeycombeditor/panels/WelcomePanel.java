package com.github.geniot.honeycombeditor.panels;

import com.github.geniot.honeycombeditor.Constants;
import com.github.geniot.honeycombeditor.EditorFrame;

import javax.swing.*;
import java.awt.*;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 08-08-2019
 */
public class WelcomePanel extends JPanel {
  private EditorFrame frame;

  public WelcomePanel(EditorFrame f) {
    this.frame = f;
    this.setName(Constants.PROP_WELCOME_COMPONENT);
    this.setLayout(new BorderLayout());
    JLabel label = new JLabel("Honeycomb Editor", JLabel.CENTER);
    add(label, BorderLayout.CENTER);
  }
}
