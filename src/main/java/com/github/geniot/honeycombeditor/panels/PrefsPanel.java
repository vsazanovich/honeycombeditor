package com.github.geniot.honeycombeditor.panels;

import com.github.geniot.honeycombeditor.Constants;
import com.github.geniot.honeycombeditor.EditorFrame;
import com.github.geniot.honeycombeditor.EditorLauncher;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 15-08-2019
 */
public class PrefsPanel extends JPanel {
  private EditorFrame frame;

  private String lafPrefix = "com.jtattoo.plaf.";
  private String[] lafs = new String[]{"Acryl", "Aero", "Aluminium", "Bernstein", "Fast", "Graphite", "HiFi", "Luna", "McWin", "Mint", "Noire", "Smart", "Texture"};


  public PrefsPanel(EditorFrame f) {
    this.frame = f;
    this.setName(Constants.PROP_PREFS_COMPONENT);
    this.setLayout(new FlowLayout());


    JComboBox lafCB = new JComboBox(lafs);
    lafCB.setSelectedIndex(getIndexByLAF(Constants.PROPS.getProperty(Constants.PROP_LAF)));
    lafCB.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        String selValue = lafCB.getSelectedItem().toString();
        String classValue = lafPrefix + selValue.toLowerCase() + "." + selValue + "LookAndFeel";
        EditorLauncher.setLAF(classValue);
      }
    });


    add(new JLabel("Look and feel: "));
    add(lafCB);

  }


  private int getIndexByLAF(String property) {
    if (property != null && !property.equals("")) {
      String[] parts = property.split("\\.");
      String name = parts[parts.length - 1].split("LookAndFeel")[0];
      for (int i = 0; i < lafs.length; i++) {
        if (lafs[i].equals(name)) {
          return i;
        }
      }
    }
    return 0;
  }
}

