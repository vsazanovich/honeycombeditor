package com.github.geniot.honeycombeditor;

import com.github.geniot.honeycombeditor.actions.LoadAction;
import com.github.geniot.honeycombeditor.actions.OnExitAction;
import com.github.geniot.honeycombeditor.actions.SaveAction;
import com.github.geniot.honeycombeditor.actions.UnloadAction;
import com.github.geniot.honeycombeditor.panels.HoneyCombPanel;
import com.github.geniot.honeycombeditor.panels.PrefsPanel;
import com.github.geniot.honeycombeditor.panels.TextEditor;
import com.github.geniot.honeycombeditor.panels.WelcomePanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.nio.file.FileSystem;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 08-08-2019
 */
public class EditorFrame extends JFrame {
  public String openFileName;
  public String shortFileName;
  public FileSystem zipFS;

  public JPanel cards;

  public WelcomePanel welcomePanel;
  public TextEditor textEditor;
  public HoneyCombPanel honeyCombPanel;
  public PrefsPanel prefsPanel;

  public LoadAction loadAction;
  public OnExitAction onExitAction;
  public SaveAction saveAction;
  public UnloadAction unloadAction;

  public String underPrefsCard;


  public EditorFrame() {
    super("Honeycomb");

    loadAction = new LoadAction(this);
    onExitAction = new OnExitAction(this);
    saveAction = new SaveAction(this);
    unloadAction = new UnloadAction(this);

    setIconImage(new ImageIcon(getClass().getClassLoader().getResource("icon.png")).getImage());

    //Display the window.
    try {
      int width = Constants.PROPS.containsKey(Constants.PROP_WIDTH) ? Integer.parseInt(Constants.PROPS.getProperty(Constants.PROP_WIDTH)) : 500;
      int height = Constants.PROPS.containsKey(Constants.PROP_HEIGHT) ? Integer.parseInt(Constants.PROPS.getProperty(Constants.PROP_HEIGHT)) : 615;
      setPreferredSize(new Dimension(width, height));
    } catch (Exception ex) {
      setPreferredSize(new Dimension(600, 800));
    }

    try {
      int posX = Constants.PROPS.containsKey(Constants.PROP_POS_X) ? Integer.parseInt(Constants.PROPS.getProperty(Constants.PROP_POS_X)) : 0;
      int posY = Constants.PROPS.containsKey(Constants.PROP_POS_Y) ? Integer.parseInt(Constants.PROPS.getProperty(Constants.PROP_POS_Y)) : 0;
      setLocation(posX, posY);
    } catch (Exception ex) {
      setLocation(0, 0);
    }

//    setResizable(false);


    KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {
      @Override
      public boolean dispatchKeyEvent(KeyEvent e) {

        if (e.getID() == KeyEvent.KEY_PRESSED && e.isControlDown() && e.getKeyCode() == KeyEvent.VK_O) {
          loadAction.actionPerformed(new ActionEvent(EditorFrame.this, 0, "open"));
        }

        if (e.getID() == KeyEvent.KEY_PRESSED && e.isControlDown() && e.getKeyCode() == KeyEvent.VK_S) {
          if (openFileName != null) {
            saveAction.actionPerformed(new ActionEvent(EditorFrame.this, 0, "save"));
          }
        }

        if (e.getID() == KeyEvent.KEY_PRESSED && e.isControlDown() && e.getKeyCode() == KeyEvent.VK_Q) {
          unloadAction.actionPerformed(new ActionEvent(EditorFrame.this, 0, "close"));
        }

        if (e.getID() == KeyEvent.KEY_PRESSED && e.getKeyCode() == KeyEvent.VK_ENTER) {
          if (getVisibleCard().equals(Constants.PROP_HONEYCOMB_COMPONENT)) {
            ((CardLayout) cards.getLayout()).show(cards, Constants.PROP_TEXTEDITOR_COMPONENT);
          }
        }

        if (e.getID() == KeyEvent.KEY_PRESSED && e.isControlDown() && e.getKeyCode() == KeyEvent.VK_P) {
          if (!getVisibleCard().equals(Constants.PROP_PREFS_COMPONENT)) {
            underPrefsCard = getVisibleCard();
            ((CardLayout) cards.getLayout()).show(cards, Constants.PROP_PREFS_COMPONENT);
          }
        }

        return false;
      }
    });
    final AbstractAction escapeAction = new AbstractAction() {
      private static final long serialVersionUID = 1L;

      @Override
      public void actionPerformed(ActionEvent ae) {
        if (getVisibleCard().equals(Constants.PROP_TEXTEDITOR_COMPONENT)) {
          if (openFileName != null) {
            saveAction.saveText();
          }
          ((CardLayout) cards.getLayout()).show(cards, Constants.PROP_HONEYCOMB_COMPONENT);
        } else if (getVisibleCard().equals(Constants.PROP_PREFS_COMPONENT)) {
          ((CardLayout) cards.getLayout()).show(cards, underPrefsCard);
        } else {
          setState(Frame.ICONIFIED);
        }
      }
    };

    getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
            .put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "ESCAPE_KEY");
    getRootPane().getActionMap().put("ESCAPE_KEY", escapeAction);


    setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    addWindowListener(onExitAction);

    welcomePanel = new WelcomePanel(this);
    textEditor = new TextEditor(this);
    honeyCombPanel = new HoneyCombPanel(this);
    prefsPanel = new PrefsPanel(this);

    cards = new JPanel(new CardLayout());
    cards.add(welcomePanel, Constants.PROP_WELCOME_COMPONENT);
    cards.add(textEditor, Constants.PROP_TEXTEDITOR_COMPONENT);
    cards.add(honeyCombPanel, Constants.PROP_HONEYCOMB_COMPONENT);
    cards.add(prefsPanel, Constants.PROP_PREFS_COMPONENT);

    setContentPane(cards);

    ((CardLayout) cards.getLayout()).show(cards, Constants.PROP_WELCOME_COMPONENT);

    load();
  }

  public void load() {
    if (Constants.PROPS.containsKey(Constants.PROP_LAST_OPEN_FILE)) {
      String lastOpenFile = Constants.PROPS.getProperty(Constants.PROP_LAST_OPEN_FILE);
      final File f = new File(lastOpenFile);
      if (f.exists()) {
        SwingUtilities.invokeLater(new Runnable() {
          @Override
          public void run() {
            loadAction.load(f);
          }
        });
      }
    }
  }


  public String getVisibleCard() {
    JPanel card = null;
    for (Component comp : cards.getComponents()) {
      if (comp.isVisible() == true) {
        card = (JPanel) comp;
      }
    }
    return card.getName();
  }


}
